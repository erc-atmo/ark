#### import the simple module from the paraview
from paraview.simple import *

source = GetActiveSource()

# create a new 'Programmable Filter'
Filter = ProgrammableFilter(Input=source)
RenameSource('Conservatives', Filter)

# Properties modified on programmableFilter1
Filter.Script = \
"""from vtk.numpy_interface import algorithms as algs

CellDatas = inputs[0].CellData

if "mz" in CellDatas.keys():
    mx = CellDatas["mx"]
    my = CellDatas["my"]
    mz = CellDatas["mz"]
    m  = algs.make_vector(mx, my, mz)
elif "my" in CellDatas.keys():
    mx = CellDatas["mx"]
    my = CellDatas["my"]
    m  = algs.make_vector(mx, my)
else:
    m  = CellDatas["mx"]

output.CellData.append(m, "m")"""

Filter.RequestInformationScript = ''
Filter.RequestUpdateExtentScript = ''
Filter.CopyArrays = 1
Filter.PythonPath = ''

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
calculator1Display = Show(Filter, renderView1)
calculator1Display.Representation = 'Outline'

# hide data in view
Hide(source, renderView1)

# update the view to ensure updated data information
renderView1.Update()

SetActiveSource(Filter)
