# Ark

## What is it ?

Provide performance portable Kokkos implementation for compressible
hydrodynamics.

## Dependencies

* [Kokkos](https://github.com/kokkos/kokkos) with version >= 2.6
* [CMake](https://cmake.org) with version >= 3.3

Optional
* [Cuda](https://developer.nvidia.com/cuda-downloads) with version >= 8.0
* MPI, for example [OpenMPI](https://www.open-mpi.org)
* [HDF5](https://portal.hdfgroup.org)
* [PnetCDF](https://github.com/Parallel-NetCDF/PnetCDF)

## How to get ark sources

Kokkos sources are included as a git submodule.

To download project "ark" clone it with option "--recurse-submodules"
```
git clone --recurse-submodules https://gitlab.erc-atmo.eu/3d_tools/ark.git
```

If you performed a regular "git clone", just type
```
git submodule init
git submodule update
```
to retrieve kokkos sources.

## How to build

A few example of builds. Default values make it compile without MPI,
with Kokkos-serial and in Release build type.

### Build Serial-Serial

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake ..
cmake --build . -- -j 4
```

Add variable CXX on the cmake command line to change the compiler
(clang++, icpc, pgcc, ....)

### Build Serial-OpenMP

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake -DKOKKOS_ENABLE_OPENMP=ON ..
cmake --build . -- -j 4
```

Add variable CXX on the cmake command line to change the compiler
(clang++, icpc, pgcc, ....)

### Build Serial-Cuda

Create a build directory, configure and make (for a Maxwell GPU, see [Table 4.2](https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables) for available architectures)
```shell
mkdir build && cd build
export CXX=$PWD/../externals/kokkos/bin/nvcc_wrapper
cmake -DKOKKOS_ENABLE_CUDA=ON -DKOKKOS_ARCH=Maxwell50 ..
cmake --build . -- -j 4
```

### Build MPI-OpenMP

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake -DUSE_MPI=ON -DKOKKOS_ENABLE_OPENMP=ON ..
cmake --build . -- -j 4
```

### Build MPI-Cuda

Please make sure to use a CUDA-aware MPI implementation (OpenMPI or
MVAPICH2) built with the proper flags for activating CUDA support.

Create a build directory, configure and make
```shell
mkdir build && cd build
export CXX=$PWD/../externals/kokkos/bin/nvcc_wrapper
cmake -DUSE_MPI=ON -DKOKKOS_ENABLE_CUDA=ON -DKOKKOS_ARCH=Maxwell50 ..
cmake --build . -- -j 4
```

## How to run

In the build directory just type
``` shell
./src/ark configuration_file.ini --kokkos-xxx=value
```
If you add kokkos options (see [Table 5.1](https://github.com/kokkos/kokkos/wiki/Initialization#table-51-command-line-options-for-kokkosinitialize-) for a list of options) from the command line, use the prefixed version (--kokkos-xxx)

### Run Serial-OpenMP

``` shell
./src/ark path/to/configuration_file.ini --kokkos-threads=2
```

### Run MPI-Cuda

To run on 4 GPUs, 1 GPU per MPI task
```shell
mpirun -np 4 ./src/ark path/to/configuration_file.ini --kokkos-ndevices=4
```

## Developping with vim and youcomplete plugin

Assuming you are using vim (or neovim) text editor and have installed
the youcomplete plugin, you can have semantic autocompletion in a C++
project.

Make sure to have CMake variable CMAKE_EXPORT_COMPILE_COMMANDS set to
ON, and symlink the generated file to the top level source directory.

## Coding rules

For cmake follow [Modern
CMake](https://cliutils.gitlab.io/modern-cmake/) which also cites
other sources like coding rules from [Effective Modern
CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1).
