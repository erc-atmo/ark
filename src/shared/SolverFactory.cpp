// Copyright CEA Saclay - Maison de la Simulation, (February 2019)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin

// thomas.padioleau@cea.fr

// This software is a computer program whose purpose is to implement
// CFD applications with the All-Regime scheme and Kokkos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "shared/SolverFactory.h"
#include "shared/SolverBase.h"
#include "muscl/SolverHydroMuscl.h"
#include "all_regime/SolverHydroAllRegime.h"

namespace ark
{

// The main solver creation routine
SolverFactory::SolverFactory()
{
    /*
     * Register some possible solvers
     */
    registerSolver("Hydro_Muscl_2D", &muscl::SolverHydroMuscl<2>::create);
    registerSolver("Hydro_Muscl_3D", &muscl::SolverHydroMuscl<3>::create);
    registerSolver("Hydro_All_Regime_2D",   &all_regime::SolverHydroAllRegime<2>::create);
    registerSolver("Hydro_All_Regime_3D",   &all_regime::SolverHydroAllRegime<3>::create);
} // SolverFactory::SolverFactory

} // namespace ark
