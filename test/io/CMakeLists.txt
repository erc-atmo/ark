configure_file(test_io_2d.ini test_io_2d.ini COPYONLY)
configure_file(test_io_3d.ini test_io_3d.ini COPYONLY)
configure_file(Xdmf.dtd Xdmf.dtd COPYONLY)

add_executable(test_io_vtk
  test_io_vtk.cpp ${CMAKE_SOURCE_DIR}/src/ark.cpp
  )

target_include_directories(test_io_vtk
  PUBLIC
  ${CMAKE_SOURCE_DIR}/kokkos/core/src
  ${CMAKE_SOURCE_DIR}/kokkos/containers/src
  ${CMAKE_SOURCE_DIR}/kokkos/algorithms/src
  ${CMAKE_BINARY_DIR}/src
  ${CMAKE_BINARY_DIR}/kokkos
  ${CMAKE_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/src/utils/io
  ${CMAKE_SOURCE_DIR}/src/utils/mpiUtils
  )
target_link_libraries(test_io_vtk
  kokkos shared config monitoring io)

if(USE_MPI)
  target_link_libraries(test_io_vtk
    mpiUtils)
endif(USE_MPI)


if(USE_HDF5)
  add_executable(test_io_hdf5
    test_io_hdf5.cpp ${CMAKE_SOURCE_DIR}/src/ark.cpp
    )
  
  target_include_directories(test_io_hdf5
    PUBLIC
    ${CMAKE_SOURCE_DIR}/kokkos/core/src
    ${CMAKE_SOURCE_DIR}/kokkos/containers/src
    ${CMAKE_SOURCE_DIR}/kokkos/algorithms/src
    ${CMAKE_BINARY_DIR}/src
    ${CMAKE_BINARY_DIR}/kokkos
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_SOURCE_DIR}/src/utils/io
    ${CMAKE_SOURCE_DIR}/src/utils/mpiUtils
    )
  target_link_libraries(test_io_hdf5
    kokkos shared config monitoring io ${HDF5_LIBRARIES})
  
  if(USE_MPI)
    target_link_libraries(test_io_hdf5
      mpiUtils)
  endif(USE_MPI)
  
endif(USE_HDF5)

if(USE_MPI)
  if(USE_PNETCDF)

    add_executable(test_io_pnetcdf
      test_io_pnetcdf.cpp ${CMAKE_SOURCE_DIR}/src/ark.cpp
      )
    
    target_include_directories(test_io_pnetcdf
      PUBLIC
      ${CMAKE_SOURCE_DIR}/kokkos/core/src
      ${CMAKE_SOURCE_DIR}/kokkos/containers/src
      ${CMAKE_SOURCE_DIR}/kokkos/algorithms/src
      ${CMAKE_BINARY_DIR}/kokkos
      ${CMAKE_SOURCE_DIR}/src
      ${CMAKE_SOURCE_DIR}/src/utils/io
      ${CMAKE_SOURCE_DIR}/src/utils/mpiUtils
      )
    target_link_libraries(test_io_pnetcdf
      kokkos shared config monitoring io mpiUtils)
    
  endif(USE_PNETCDF)
endif(USE_MPI)

if(USE_HDF5)
  if(USE_MPI)
    add_test(
      NAME generate_2d_hdf5
      COMMAND mpirun --allow-run-as-root -np 6 ${CMAKE_CURRENT_BINARY_DIR}/test_io_hdf5 ${CMAKE_CURRENT_BINARY_DIR}/test_io_2d.ini
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  else(USE_MPI)
    add_test(
      NAME generate_2d_hdf5
      COMMAND ${CMAKE_CURRENT_BINARY_DIR}/test_io_hdf5 ${CMAKE_CURRENT_BINARY_DIR}/test_io_2d.ini
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  endif(USE_MPI)

  add_test(
    NAME compare_2d_hdf5
    COMMAND h5diff ${CMAKE_CURRENT_BINARY_DIR}/output2d_0000000.h5 ${CMAKE_CURRENT_BINARY_DIR}/output2d_save_0000000.h5)
  set_tests_properties(compare_2d_hdf5 PROPERTIES DEPENDS generate_2d_hdf5)

  add_test(
    NAME validate_2d_xml
    COMMAND xmllint --xinclude --dtdvalid Xdmf.dtd ${CMAKE_CURRENT_BINARY_DIR}/output2d.xmf ${CMAKE_CURRENT_BINARY_DIR}/output2d_save.xmf)
  set_tests_properties(validate_2d_xml PROPERTIES DEPENDS generate_2d_hdf5)

  if(USE_MPI)
    # add_test(
    #   NAME generate_3d_hdf5
    #   COMMAND mpirun --allow-run-as-root -np 6 ${CMAKE_CURRENT_BINARY_DIR}/test_io_hdf5 ${CMAKE_CURRENT_BINARY_DIR}/test_io_3d.ini
    #   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  else(USE_MPI)
    add_test(
      NAME generate_3d_hdf5
      COMMAND ${CMAKE_CURRENT_BINARY_DIR}/test_io_hdf5 ${CMAKE_CURRENT_BINARY_DIR}/test_io_3d.ini
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    add_test(
      NAME compare_3d_hdf5
      COMMAND h5diff ${CMAKE_CURRENT_BINARY_DIR}/output3d_0000000.h5 ${CMAKE_CURRENT_BINARY_DIR}/output3d_save_0000000.h5)
    set_tests_properties(compare_3d_hdf5 PROPERTIES DEPENDS generate_3d_hdf5)
    add_test(
      NAME validate_3d_xml
      COMMAND xmllint --xinclude --dtdvalid Xdmf.dtd ${CMAKE_CURRENT_BINARY_DIR}/output3d.xmf ${CMAKE_CURRENT_BINARY_DIR}/output3d_save.xmf)
    set_tests_properties(validate_3d_xml PROPERTIES DEPENDS generate_3d_hdf5)
  endif(USE_MPI)

  # add_test(
  #   NAME compare_3d_hdf5
  #   COMMAND h5diff ${CMAKE_CURRENT_BINARY_DIR}/output3d_0000000.h5 ${CMAKE_CURRENT_BINARY_DIR}/output3d_save_0000000.h5)
  # set_tests_properties(compare_3d_hdf5 PROPERTIES DEPENDS generate_3d_hdf5)
endif(USE_HDF5)


if(USE_PNETCDF)
  if(USE_MPI)
    add_test(
      NAME test_2d_pnetcdf
      COMMAND mpirun --allow-run-as-root -np 6 ${CMAKE_CURRENT_BINARY_DIR}/test_io_pnetcdf ${CMAKE_CURRENT_BINARY_DIR}/test_io_2d.ini
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    # add_test(
    #   NAME test_3d_pnetcdf
    #   COMMAND mpirun --allow-run-as-root -np 6 ${CMAKE_CURRENT_BINARY_DIR}/test_io_pnetcdf ${CMAKE_CURRENT_BINARY_DIR}/test_io_3d.ini
    #   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    endif(USE_MPI)
endif(USE_PNETCDF)
